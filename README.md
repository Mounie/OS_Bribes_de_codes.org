# Bribes de codes illustrant certains points en systèmes d'exploitation

## Bribes de codes en org-mode

Le but est de fournir des bribes de code servant à illustrer des
concepts des systèmes d'exploitation, ou à donner des exemples simples
et avancés de ces concepts.

L'ensemble des codes, textes et commentaires sont sous licence
[*Creative Common CC-BY-SA*](http://creativecommons.org/licenses/by-sa/3.0/fr/legalcode).
