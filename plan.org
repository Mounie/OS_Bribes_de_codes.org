

#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t c:nil creator:nil d:(not "LOGBOOK") date:t e:t
#+OPTIONS: email:nil f:t inline:t num:t p:nil pri:nil prop:nil stat:t
#+OPTIONS: tags:t tasks:t tex:t timestamp:t title:t toc:t todo:t |:t
#+TITLE: OS: bribes de codes
#+DATE: <2016-01-26 mar.>
#+AUTHOR: Grégory Mounié
#+EMAIL: Grégory.Mounie@imag.fr
#+LANGUAGE: en
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 24.5.1 (Org mode 8.3.2)

* Introduction, divers
  - [[./intro.org][Explications et autres introductions]]
* Processus
  - [[./listeps.org][Listes de processus]]
* Mécanismes de synchronisation et réalisation des processus
  - Incrémenter un entier à plusieurs
  - Le conte de fées: Dekker et Peterson
* Les interblocages
* Gestion mémoire 
* Pagination 
* Gestion de fichiers
* Les systèmes distribués 
* Les multi-processeurs 

